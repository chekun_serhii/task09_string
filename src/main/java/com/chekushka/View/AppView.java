package com.chekushka.View;

import com.chekushka.Controller.Controller;
import com.chekushka.Controller.ControllerImpl;
import com.chekushka.View.Entity.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class AppView {
    private static Logger logger = LogManager.getLogger(AppView.class);
    Scanner input = new Scanner(System.in);
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapExecution;
    private Locale locale;
    private ResourceBundle bundle;
    private Controller controller = new ControllerImpl();

    private void setMenu() {
        menuMap = new LinkedHashMap<>();
        menuMap.put("1", bundle.getString("1"));
        menuMap.put("2", bundle.getString("2"));
        menuMap.put("3", bundle.getString("3"));
        menuMap.put("Q", bundle.getString("Q"));

    }

    public AppView() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        menuMapExecution = new LinkedHashMap<>();
        menuMapExecution.put("1", this::setLanguage);
        menuMapExecution.put("2", controller::doFileRead);
        menuMapExecution.put("3", controller::doTaskOne);
    }

    private void menu() {
        logger.info("\nTask 9. Strings.");
        setMenu();
        for (String str : menuMap.values()) {
            logger.info(str);
        }
    }

    public void start() {
        String key;
        do {
            menu();
            logger.info("Please select an option:");
            key = input.next();
            try {
                menuMapExecution.get(key).print();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        } while (!key.equalsIgnoreCase("q"));
    }

    private void setLanguage(){
        logger.info("1.English\n"
                + "2.Ukrainian\n"
                + "3.Japanese\n"
                + "4.Czech\n"
                + "Choose language:");
        int choose = input.nextInt();
        if(choose == 1){
            locale = new Locale("en");
            bundle = ResourceBundle.getBundle("Menu", locale);
            setMenu();
            start();
        }
        else if(choose == 2){
            locale = new Locale("uk");
            bundle = ResourceBundle.getBundle("Menu", locale);
            setMenu();
            start();
        }
        else if(choose == 3){
            locale = new Locale("ja");
            bundle = ResourceBundle.getBundle("Menu", locale);
            setMenu();
            start();
        }
        else if(choose == 4){
            locale = new Locale("cs");
            bundle = ResourceBundle.getBundle("Menu", locale);
            setMenu();
            start();
        }
    }
}
