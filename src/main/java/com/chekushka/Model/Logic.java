package com.chekushka.Model;

import java.util.List;

public interface Logic {
    List<String> fileRead();
    List<String> cleanText(List<String> list);
    int numSentenceUseSameWords(String text);
    void toBiggerSentence(String text);
    String findUniqueWord(String text);

}
