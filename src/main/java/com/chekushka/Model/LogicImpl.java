package com.chekushka.Model;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LogicImpl implements Logic {

    @Override
    public List<String> fileRead() {
        List<String> text = new ArrayList<>();
        try {
            text = Files.lines(Paths.get("text.txt"), StandardCharsets.UTF_8)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.getMessage();
        }
        return text;
    }

    @Override
    public List<String> cleanText(List<String> list) {
        List<String> newList = new ArrayList<>();
        for (String str : list) {
            newList.add(str.replaceAll("\\s+ | \\t+", " "));
        }
        return newList;
    }

    @Override
    public int numSentenceUseSameWords(String text) {
        int counter = 0;
        Pattern p = Pattern.compile("(?i)\\\\b([a-z]+)\\\\b(?:\\\\s+\\\\1\\\\b)+", Pattern.MULTILINE + Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        if (!m.find())
             return 0;
        else {
            while (m.find()) {
                counter++;
            }
        }
        return counter;
    }

    @Override
    public void toBiggerSentence(String text) {

    }

    @Override
    public String findUniqueWord(String text) {
        return null;
    }

}
