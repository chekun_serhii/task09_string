package com.chekushka.Controller;

import com.chekushka.Model.LogicImpl;

import java.util.ArrayList;
import java.util.List;

public class ControllerImpl implements Controller {
    private LogicImpl logic = new LogicImpl();
    private List<String> list = new ArrayList<>();

    @Override
    public void doFileRead() {
        list = logic.fileRead();
        list = logic.cleanText(list);
    }

    @Override
    public int doTaskOne() {
        int num = 0;
        for (String s : list) {
            logic.numSentenceUseSameWords(s);
        }
        return num;
    }

    @Override
    public void doTaskTwo() {

    }

    @Override
    public void doTaskTree() {

    }
}
