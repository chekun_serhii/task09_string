package com.chekushka.Controller;

public interface Controller {
    void doFileRead();
    int doTaskOne();
    void doTaskTwo();
    void doTaskTree();
}
